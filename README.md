# Clase 3

Materiales para la Tercera clase, en esta clase cubrimos:

- Uso de Flujos (Streams) en Java aprovechando la programación funcional y sus distintas herramientas.

## Lista de Materiales

- [Apunte 04 - API de Streams](./funcional/README.md)

## Lista de Ejemplos

- [Ejemplos usando streams](./funcional/)

## Estado general de la semana

Versión para publicación 2023.

***

## Software requerido

- Java
- Maven
- IntelliJ idea

## Clonar el presente repositorio

``` bash
cd existing_repo
git remote add origin https://gitlab.com/felisteffo.dev/arg-programa/clase-3/lectura-3.git
git branch -M main
git push -uf origin main
```

## Autores

Felipe Steffolani - basado en un material original de Diego Serrano para la Cátedra de Backend de Aplicaciones

## License

Este trabajo está licenciado bajo una Licencia Creative Commons Atribución-NoComercial-CompartirIgual 4.0 Internacional. Para ver una copia de esta licencia, visita [https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es](!https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es).
