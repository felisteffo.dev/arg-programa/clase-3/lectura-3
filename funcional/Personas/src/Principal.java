import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;

public class Principal {

    public static void main(String[] args) throws IOException {

        List<Persona> plantel = Files.lines(Paths.get("personas.csv"))
                .map(Persona::new)
                .toList();

        System.out.println("Nombre y apellido de todos las personas que tienen entre 10 y 20 años");
        plantel.stream()
                .filter(persona -> persona.getEdad() > 10 && persona.getEdad() < 20)
                .map(Persona::nombreCompleto)
                .forEach(System.out::println);

        System.out.println("toString de todos los PEREZ ordenados por nombre");
        plantel.stream()
                .filter(p -> p.getApellido().equals("PEREZ"))
                .sorted(Comparator.comparing(Persona::getNombre))
                .forEach(System.out::println);

        double promedio = plantel.stream().mapToInt(Persona::getEdad).average().getAsDouble();
        System.out.println("Promedio de las edades de todas las personas: " + promedio);

    }
}
