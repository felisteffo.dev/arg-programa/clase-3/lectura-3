import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLOutput;
import java.util.List;

public class Principal {
    public static void main(String[] args) throws IOException {


        List<Integer> numeros = Files.lines(Paths.get("numeros.txt"))
                .map(Integer::valueOf)
                .toList();

        // Listado de todos los numeros
        System.out.println("Listado de todos los numeros:");
        numeros.stream().forEach(System.out::println);

        // Cantidad de pares
        long cantidad = numeros.stream().filter(x -> x % 2 == 0).count();
        System.out.println("Hay " + cantidad + " pares");

        // Raices cuadradas de los 20 menores
        numeros.stream().sorted().limit(20).map(Math::sqrt).forEach(System.out::println);

    }
}
