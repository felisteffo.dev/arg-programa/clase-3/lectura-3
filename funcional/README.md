
# Apunte 4 - API de Streams

## Stream API

La Stream API o API de flujos permite manipular y procesar conjuntos de datos como una secuencia o flujo.
A diferencia de las colecciones que mantienen todos los datos almacenados en memoria y que pueden ser recorridos
mediante ciclos, los flujos plantean un concepto en cierto modo opuesto. Un stream es una secuencia de datos que
van "transcurriendo", sin estar necesariamente almacenados todos juntos. Y sobre esa secuencia el programador introduce operaciones que manipulen cada dato individualmente, eliminandolo de la responsabilidad de recorrerlos.

Puede imaginarse a un flujo como una cascada, en la cual cada gota de agua es cada uno de los datos. Al caer el agua desde la cascada puede cada gota individualmente ser interceptada y desviada por las piedras o ramas existentes, pero finalmente todas las gotas finalizan cayendo en una laguna. Siguiendo esta analogía, las operaciones que se insertan en el flujo son las interrupciones que las gotas encuentran en su caída.

## Obtención de flujos

La API permite crear flujos desde diversos orígenes:

* Desde colecciones: la interfaz Collection ofrece un método stream() que instancia un nuevo flujo
con los datos almacenados en la coleccion. Es el mecanismo más utilizado para generar nuevos flujos.
* Desde arreglos: el método Arrays.stream() recibe como parámetro un arreglo y genera un flujo con los
datos del mismo.
* Desde valores fijos: el método Stream.of() recibe una serie de objetos separados por comas y genera
un nuevo flujo con esos valores.
* Flujos infinitos con iterate(): el método Stream.iterate() recibe dos parámetros: un valor inicial y un UnaryOperator
del mismo tipo que el valor inicial. El método genera un flujo infinito en el que el primer valor 
es el valor inicial indicado en el parámetro y los siguiente se obtienen con el resultado de
aplicar el unary operator al valor anterior. Dado que el flujo es infinito, debe ser interrupido con
la operación intermedia limit()
* Flujos infinitos con generate(): el método Stream.generate() recibe un Supplier y genera un flujo 
cuyos valores son los resultados de llamar repetidamente al Supplier. También debe ser interrumpido con limit().
* Métodos generadores: algunos objetos permite generar flujos particulares según sus responsabilidades. 
Por ejemplo: el método Files.lines() genera un flujo de String con cada línea de un archivo de texto, o  el método 
getResultStream() de JPA que retorna un flujo de entidades sin que ocupen nunca memoria del proceso, como sí lo hace 
getResultList().

## Operaciones intermedias

Las operaciones intermedias procesan cada uno de los elementos del flujo con algún criterio u objetivo y retornan otro
flujo con el resultado de la operación. De esta manera se pueden encadenar más de una operación intermedia, las cuales
se ejecutan en orden para cada uno de los elementos del flujo. Habitualmente las operaciones intermedia reciben como 
parámetros instancias de interfaces funcionales, las cuales pueden ser redactadas como funciones lambda.

A continuación se presentan las operaciones intermedias más habituales:

### filter

La operación intermedia filter recibe como parámetro un predicado, es decir una función lambda que recibe un parámetro 
y retorna un boolean. Filter invoca a la función lambda una vez por cada elemento del flujo pasándoselo como parámetro y 
retorna un nuevo flujo que contiene cada uno de los elementos para los que la función retornó verdadero.

```java
List<Integer> numeros = new ArrayList<>();
// ...
// llenar la lista con números
///...

Stream<Integer> pares = numeros.stream().filter(x -> x % 2 == 0);

// pares es un flujo que contendrá
// los mismos números de la lista original
// pero sólo si son pares.
```


### distinct()

La operación intermedia distinct() retorna un nuevo flujo con los mismos datos del flujo original
pero eliminando los repetidos.

### map

La operación intermedia map() recibe como parámetro una instancia de la interfaz funcional Function
de forma tal que genere un nuevo valor (incluso posiblemente de otro tipo) a partir de cada uno de los
elementos del flujo.

```java
List<Integer> numeros = new ArrayList<>();
// ...
// llenar la lista con números
///...

Stream<Integer> cuadrados = numeros.stream().map(x -> x * x);
Stream<Double> raices = numeros.stream().map(x -> Math.sqrt(x))
// cuadrados es un flujo que contendrá
// los cuadrados de todos los números de la lista original
// mientras que raices contiene la raíz cuadrada de cada uno
```

### limit

Recibe un número entero y genera un nuevo flujo que contiene como máximo la cantidad indicada. Es especialmente útil
para los flujos infinitos:

```java
Random r = new Random();
Stream<Integer> aleatorios = Stream.generate(() -> r.nextInt()).limit(100);
/// aleatorios contendrá 100 números enteros generados al azar.
```

### sorted

Retorna un nuevo flujo con los mismos elementos que el original pero ordenados. Si el flujo es de objetos 
que implementan Comparable los ordena según el resultado del método compareTo. Tambien puede recibir un objeto
Comparator y los ordena con el resultado del método compare.

## Operaciones terminales

Las operaciones terminales recolectan todos los elementos de un flujo y los procesan pero sin retornar un nuevo flujo.
Algunas operaciones realizan una operaciones y no retornan nada mientras que otras obtienen un resultado. Es importante
destacar que toda manipulación de un flujo debe finalizar siempre con una operación terminal. Las operaciones intermedias
no recorren el conjunto de datos ni ejecutan nada hasta que no se ejecuta una operación terminal.

Las operaciones terminales más usadas son las siguientes:

### count

Retorna la cantidad de elementos del flujo.

### forEach

Ejecuta un Consumer por cada elemento. El consumer recibe como parámetro el dato y no retorna nada

```java
pares.forEach(x -> System.out.println(x));
// Imprime cada uno de los números del flujo pares.
```

### toList

Instancia una lista y la llena con todos los elementos del flujo.

### reduce

Recibe un BinaryOperator y efectúa una operación de reducción o pliegue con todos los elementos del flujo. 
La reducción repite una operación entre todos los elementos obteniendo un único resultado. Para ello toma los dos primeros 
elementos del flujo y le aplica la operación (al ser un BinaryOperator la operacion recibe dos datos y retorna un resultado
del mismo tipo), luego toma el tercero y repite la operación entre este y el resultado anterior. Luego procede 
de la misma manera con todos los elementos existentes hasta que el flujo finalice

```java
int sumaCuadrados = cuadrados.reduce((a, b) -> a + b);
// Reduce suma los dos primeros números, que se reciben en los parámetros a y b
// Luego obtiene el tercero y vuelve a ejecutarse con a igual a la suma anterior
// y b con el valor del tercero. Desde ahí repite.
```

### min / max

Con el mismo criterio que la operación sorted, obtienen respectivamente el menor y mayor valor del flujo

### anyMatch / allMatch / noneMatch

Estas operaciones intermedias reciben un predicado y retornan un boolean. La operación anyMatch retorna
verdadero si algún elemento del flujo cumple con el predicado, allMatch si todos cumplen y noneMatch si para ningún elemento
el predicado se hace verdadero.


```java
if (aleatorios.anyMatch(x -> x > 100)) {
    System.out.println("Hay algún número mayor a 100");
    if (aleatorios.allMatch(x -> x > 100))
        System.out.println("Y son todos mayores a 100");
}
```


# Referencias a métodos

Las referencias a métodos, también conocidas como "method references" en inglés, son una característica de programación funcional que está presente en lenguajes como Java, que soportan este paradigma.

En Java, las referencias a métodos permiten pasar una referencia a un método como argumento a una función de orden superior o a un método en lugar de tener que definir una expresión lambda para ese método. Es una forma más concisa y legible de expresar funciones o comportamientos que ya están definidos en algún lugar del código.

Existen cuatro tipos de referencias a métodos en Java:

  * Referencias a métodos estáticos: Permiten referenciar a métodos estáticos de una clase.

```java
// Sintaxis: Clase::métodoEstático
MiClase::metodoEstatico
```
  * Referencias a métodos de instancia de un objeto particular: Permiten referenciar a métodos de instancia de un objeto específico.

```java
// Sintaxis: objeto::métodoDeInstancia
miObjeto::metodoDeInstancia
```
  * Referencias a métodos de instancia de un tipo arbitrario: Permiten referenciar a métodos de instancia de cualquier objeto de un tipo determinado.

```java
// Sintaxis: Tipo::métodoDeInstancia
MiClase::metodoDeInstancia
```
  * Referencias a constructores: Permiten referenciar a constructores de clases.

```java
// Sintaxis: Tipo::new
MiClase::new
```

Estas referencias se pueden utilizar en contextos donde se espera una expresión lambda, siempre que el método al que se hace referencia tenga la misma firma que la función esperada. Es importante destacar que las referencias a métodos no ejecutan el método en el momento de definición, sino que proporcionan una referencia al mismo para que se pueda invocar más adelante.


## Ejemplo: Listado por pantalla

Para mostrar por pantalla todo el contenido de un flujo se puede utilizar la operación terminal forEach, que recibe un Supplier. Por lo tanto puede pasarse cualquier expresión lambda que reciba un parámetro y no retorne nada. El método println del objeto System.out cumple con esa firma, entonces el uso sería:


```java
// Generación de una lista de 100 números aleatorios enteros entre 1 y 100
List<Integer> numeros = new Random().ints(100,1,1000).boxed().toList();
// Listado por pantalla
numeros.stream().forEach(x => System.out.println(x));
```

En este caso, dado que el único parámetro formal de la expresión lambda es el único parametro actual del método println, se puede enviar una referencia a dicho método, sin necesidad de hacer llamadas explícitas al mismo:

```java
numeros.stream().forEach(System.out::println);
```

## Ejemplo: Lectura de un archivo

Si se dispone de un archivo de texto que contiene un dato individual por cada línea se puede aprovechar la api de flujos para leerlo y obtener objetos a partir de su contenido de una forma ágil, en muy pocas líneas de código y sin usar ninguna estructura de control.

La clase Files provee el método estático readAllLines recibe el nombre de un archivo y devuelve una lista de String con el contenido de todas las líneas del archivo. Si se dispone de algún método que reciba un String y retorne una instancia de alguna clase cuyo estado pueda ser llenado interpretando el String, se puede utilizar la operación intermedia map para convertir el flujo de cadenas en un flujo de objetos:


```java
List<Integer> numeros = Files.lines(Paths.get("numeros.txt"))
                             .map(Integer::valueOf)
                             .toList();
```

De la misma manera, si una clase posee un constructor que recibe un objeto (de la misma clase u otra) y con sus atributos pueda crear una nueva instancia, se puede pasar una referencia a
dicho constructor. Un caso habitual se da al leer un archivo de valores separados por comas. En el siguiente ejemplo se dispone de un archivo que en cada línea posee los datos de una persona, separados por
caracteres de punto y coma.

Si se le agrega a la clase Persona un constructor que reciba un String y lo interprete para asignar los valores de sus atributos, todo el proceso de la lectura del archivo se reduce a una línea:


```java
// En la clase Persona:
public Persona(String linea) {
    String[]valores = linea.split(";");
    this.documento = Integer.valueOf(valores[0]);
    this.nombre = valores[1];
    this.apellido = valores[2];
    this.edad = Integer.valueOf(valores[3]);
}

// En la clase que lee el archivo
List<Persona> plantel = Files.lines(Paths.get("personas.csv"))
                             .map(Persona::new)
                             .toList();
```

## Ejemplos

- [Procesamiento del archivo de numeros](./Numeros)
- [Procesamiento del archivo de personas](./Personas)
